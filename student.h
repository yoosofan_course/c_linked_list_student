/*!
 * \author Ahmad Yoosofan <yoosofan@mail.com> <yoosofan@myfastmail.com>
 * \version 0.0.1
 * \date 2018
 * \copyright View only (not copy, not use, not even compile) just for the course in the university of Kashan by Ahmad Yoosofan
*/

#ifndef STUDENT_H
#define STUDENT_H 1
typedef struct studentX
{char name[50],stdno[15];}student;
student input_student(void);
void print_student(student);
#endif
