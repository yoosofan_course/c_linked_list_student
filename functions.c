/*!
 * \author Ahmad Yoosofan <yoosofan@mail.com> <yoosofan@myfastmail.com>
 * \version 0.0.1
 * \date 2018
 * \copyright View only (not copy, not use, not even compile) just for the course in the university of Kashan by Ahmad Yoosofan
*/

#include "student.h"
#include "functions.h"
#include "linked_list_student.h"
void f1(void){
	Node_student head;
	student st1,st2;
	head.next=0;// NULL
	st1=input_student();
	st2=input_student();
	head=insert_first_student(head,st1);
	head=insert_first_student(head,st2);
	print_linklist_student(head);
	
}	
