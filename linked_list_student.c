/*!
 * \author Ahmad Yoosofan <yoosofan@mail.com> <yoosofan@myfastmail.com>
 * \version 0.0.1
 * \date 2018
 * \copyright View only (not copy, not use, not even compile) just for the course in the university of Kashan by Ahmad Yoosofan
*/

#include "linked_list_student.h"
#include <stdlib.h>
#include <stdio.h>
void print_linklist_student(Node_student h){
	Node_student*p1=h.next;
	for(;p1;p1=p1->next)
		print_student(p1->st);
}		
Node_student insert_first_student(Node_student h, student st){
	Node_student*p1;
	p1=h.next;
	h.next=(Node_student*)malloc(sizeof(Node_student));
	if(!h.next){printf("Cannot allocate memory\n");exit(0);}
	h.next->next=p1;
	h.next->st=st;
	return h;
}
//void append_student(Node_student,student){}
