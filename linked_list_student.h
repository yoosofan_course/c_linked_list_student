/*!
 * \author Ahmad Yoosofan <yoosofan@mail.com> <yoosofan@myfastmail.com>
 * \version 0.0.1
 * \date 2018
 * \copyright View only (not copy, not use, not even compile) just for the course in the university of Kashan by Ahmad Yoosofan
*/

#ifndef LINKED_LIST_STUDENT
#define LINKED_LIST_STUDENT 1454
#include "student.h"
typedef struct Node_studentX{
	student st;
	struct Node_studentX * next;
}Node_student;
Node_student insert_first_student(Node_student h, student st);
//void append_student(Node_student,student);
void print_linklist_student(Node_student );
#endif
